# Welcome to TuCSoN4JADE Repository

---

### What is TuCSoN4JADE?

**TuCSoN4JADE** (**t4j** for short) is a *Java library* enabling *JADE* agents to exploit *TuCSoN* coordination services wrapped as an ad-hoc API into a JADE kernel service. **[JADE](http://jade.tilab.com)** is a *FIPA-compliant* *Java-based* agent development framework featuring standard MAS middleware features such as agent mobility, white and yellow pages service, ACL-based message passing, and built-in FIPA protocols. **[TuCSoN](http://tucson.unibo.it)** is *Java-based* middleware providing software agents with coordination as a service via programmable logic tuple spaces, called *tuple centres*.

By combining TuCSoN and JADE, t4j aims at providing MAS engineers with a full-featured MAS middleware, enabling them to exploit both dimensions of *agent-oriented software engineering* — individual, through JADE agents; social, via TuCSoN tuple centres — in a complete and well-balanced way.

t4j is available under [GNU LGPL license](https://www.gnu.org/licenses/lgpl.html).

Main t4j web page is <http://tucson4jade.apice.unibo.it/>.

---