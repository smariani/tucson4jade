# TuCSoN4JADE "how-to"

In this brief "how-to", you will learn how to get TuCSoN4JADE (**t4j** for short), build it and run a first example showcasing its features.

Assumptions are you are familiar with Java (compilation), JADE (usage), Git (cloning repositories) and, optionally, ANT (buildfiles).

---

1. <a href="#getting">Getting Started with t4j</a>
   
   1.1 <a href="#downloading">Downloading</a>
   
   1.2 <a href="#compiling">Compiling</a>
   
   1.3 <a href="#deploying">Deploying</a>
   
   1.4 <a href="#running">Running</a>
      
2. <a href="#contact">Contact Information</a>

---

### 1. <a name="getting">Getting Started with t4j</a>

---

###### 1.1 <a name="downloading">Downloading</a>

If you want the *ready-to-use* distribution of t4j, download **t4j.jar** archive from the "Downloads" section, here > <http://bitbucket.org/smariani/tucson4jade/downloads>.

If you want the *source code* of t4j, clone the t4j **[Git](http://git-scm.com) repository** hosted here on Bitbucket, at > <http://smariani@bitbucket.org/smariani/tucson4jade.git> (e.g., from a command prompt type `$> git clone https://smariani@bitbucket.org/smariani/tucson4jade.git`) <a href="#1">\[1\]</a>.

In the former case, skip to the "<a href="#running">Running</a>" section below. In the latter case, keep reading.

---

###### 1.2 <a name="compiling">Compiling</a>

By cloning t4j you have downloaded a folder named `tucson4jade/`, with the following *directory structure*:

    tucson4jade/
    |__...
    |__t4j/
       |__...
       |__ant-scripts/
          |__build.xml
          |__environment.properties
       |__eclipse-config/
       |__how-to/
       |__license-info/
       |__src/

t4j depends on 3 other Java libraries to function properly <a href="#2">\[2\]</a>:

 * **JADE**, downloadable from JADE "Download" page, here > <http://jade.tilab.com/download/jade/> (**jade.jar**)
 * **TuCSoN**, downloadable from TuCSoN "Downloads" section, here > <http://bitbucket.org/smariani/tucson/downloads> (**tucson.jar**)
 * **tuProlog**, downloadable from tuProlog "Download" page, here > <http://apice.unibo.it/xwiki/bin/view/Tuprolog/Download> (**2p.jar**)
 
Once you got the above libraries, you are ready to compile t4j source code. 

The easiest way to do so is by exploiting the [ANT](http://ant.apache.org) script named `build.xml` within folder `ant-scripts/`, which takes care of the whole building process for you, from compilation to deployment (covered in next section). To do so, you need to have ANT installed on your machine <a href="#3">\[3\]</a>. If you don't want to use ANT, build t4j jar archive using the tools you prefer, then skip to the "<a href="#running">Running</a>" section below.

To compile t4j using ANT:

 1. Edit the `environment.properties` file according to your system configuration:
 
    1.1 Tell ANT where your JDK and your `java` tool are
    
    1.2 Tell ANT which libraries are needed to compile t4j (those you just downloaded, that is JADE, TuCSoN and tuProlog)
    
    1.3 Tell ANT where you put such libraries (e.g. if you put them into `t4j/libs/` you are already set)
    
    *\[1.4 Tell ANT your Bitbucket username (for automatic syncing with t4j repository, **not supported at the moment**)\]*
 
 2. Launch the ANT script using target `compile` (e.g., from a command prompt position yourself into the `ant-scripts/` folder then type `$> ant compile`) <a href="#4">\[4\]</a>. This will create folder `classes/` within folder `t4j/` and therein store Java `.class` files.
 
Other ANT targets are available through the `build.xml` file: to learn which, launch the ANT script using target `help`.

---

###### 1.3 <a name="deploying">Deploying</a>

Deploying t4j is as simple as giving a different build target to the ANT script `build.xml`:

 * if you only want the **t4j jar** archive, ready to be included in your JADE project, launch the script using target `lib`. This will compile t4j source code into binaries (put into `t4j/classes/` folder) then package them to **t4j.jar** into `t4j/lib/` folder <a href="#5">\[5\]</a>.
 
 * if you want a **ready-to-release distribution** of t4j, including also documentation and support libraries, launch the script using target `dist`. This will:
   
   * compile t4j source code into binaries, put into `t4j/classes/` folder
   * package them to t4j.jar, put into `t4j/lib/` folder
   * generate Javadoc information, put into `t4j/doc/` folder
   * create folder `jade/add-ons/TuCSoN4JADE-${version}` including:
   
     * folder `docs/` including the generated Javadoc information as well as this "how-to"
     * folder `libs/` including JADE, TuCSoN and tuProlog libraries used to build t4j
     * folder `rel/` including t4j jar archives
     
The complete directory structure obtained by launching `ant dist` build process should look like the following (assuming you put JADE, TuCSoN and tuProlog libraries in folder `t4j/libs/`):

    tucson4jade/
    |__...
    |__t4j/
       |__...
       |__ant-scripts/
          |__build.xml
          |__environment.properties
       |__classes/
       |__doc/
       |__eclipse-config/
       |__how-to/
       |__jade/
          |__add-ons/
             |__TuCSoN4JADE-${version}/
                |__docs/
                   |__how-to/
                   |__javadoc/
                |__libs/
                |__rel/
             |__...
       |__lib/
       |__libs/
       |__license-info/
       |__src/

Other ANT targets are available through the `build.xml` file: to learn which, launch the ANT script using target `help`.

---

###### 1.4 <a name="running">Running</a>

To run t4j, you need:

 * t4j jar, **t4j.jar**
 * JADE jar, **jade.jar**
 * TuCSoN jar, **tucson.jar**
 * tuProlog jar, **2p.jar**

Supposing you built t4j using the provided ANT script <a href="#6">\[6\]</a> and that you are comfortable with using a command prompt to launch Java applications <a href="#7">\[7\]</a>:

 1. open a command prompt and position yourself into either `t4j/lib/` or `t4j/jade/add-ons/TuCSoN4JADE-${version}/rel/` folder
 2. launch the JADE platform specifying you want to exploit TuCSoN services provided by t4j, e.g. as follows <a href="#8">\[8\]</a>:
 
         java -cp t4j.jar:../libs/tucson.jar:../libs/2p.jar:../libs/jade.jar jade.Boot -gui -services it.unibo.tucson.jade.service.TucsonService

JADE GUI should appear as well as the t4j ASCII logo on the command prompt, as depicted below.

<img src="t4j-running.jpeg" alt="t4j in execution">

As long as no JADE agents start exploiting TuCSoN coordination services wrapped by t4j, nothing happens. Thus, here follows instructions on how to launch the example application shipped within t4j.jar, showcasing its features: the "Book Trading" MAS (package `it.unibo.tucson.jade.examples.bookTrading`).

Supposing you successfully launched the JADE platform as described above, to launch the "Book Trading" example:
      
 1. operate on JADE gui to launch one "seller agent" (`it.unibo.tucson.jade.examples.bookTrading.BookSellerAgent`), whose name should adhere to TuCSoN agents naming rules (roughly, lowercase letters <a href="#9">\[9\]</a>)
   
 2. operate on JADE gui to launch at least one "buyer agent" (`it.unibo.tucson.jade.examples.bookTrading.BookBuyerAgent`), whose name should adhere to TuCSoN agents naming rules

You should see many prints on the command prompt, tracking what happens in the MAS <a href="#10">\[10\]</a>.

**NB:** To showcase TuCSoN4JADE features, the example is designed to start & stop a *default* TuCSoN node (from the seller agent), thus:

 * launching more seller agents will likely raise exceptions
 * starting a buyer agent first will likely raise exceptions
 * shutting the seller agent prior to the buyer agent will likely raise exceptions
 
If you want to try a setting with more sellers, comment out node starting/stopping code in seller agent's source code and start the TuCSoN node separately <a href="#11">\[11\]</a>.

---

### <a name="contact">Contact Information</a>

**Author** of this "how-to":

 * *Stefano Mariani*, DISI - Università di Bologna (<s.mariani@unibo.it>)

**Authors** of the add-on:

 * *Stefano Mariani*, DISI - Università di Bologna (<s.mariani@unibo.it>)
 * Luca Sangiorgi, Università di Bologna
 * Prof. Andrea Omicini, DISI - Università di Bologna
 
---

<a name="1">\[1\]</a> Git standalone clients are available for any platform (e.g., [SourceTree](http://www.sourcetreeapp.com) for Mac OS and Windows). Also, if you are using [Eclipse IDE](http://www.eclipse.org/home/index.php) for developing in JADE, the [EGit plugin](http://marketplace.eclipse.org/content/egit-git-team-provider) is included in the [Java Developers version](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/lunasr1) of the IDE.

<a name="2">\[2\]</a> Recommended JADE version is **4.3.2**. Regarding TuCSoN and tuProlog, recommended version is **1.11.0.0209** and **2.9.1**, respectively. Others (both newer and older) may work properly, but they have not been tested.

<a name="3">\[3\]</a> Binaries available [here](http://ant.apache.org/bindownload.cgi), installation instructions covering Linux, MacOS X, Windows and Unix systems [here](http://ant.apache.org/manual/install.html).

<a name="4">\[4\]</a> If you are using [Eclipse IDE](http://www.eclipse.org/home/index.php) for developing in JADE, ANT is included: click "Window > Show View > Ant" then click "Add buildfiles" from the ANT view and select file `build.xml` within `ant-scripts/` folder. Now expand the "TuCSoN4JADE build file" from the ANT view and finally double click on target `compile` to start the build process.

<a name="5">\[5\]</a> Actually, also a **t4j-noexamples.jar** is built. It is the same as **t4j.jar** except for the explanatory example in package `it.unibo.tucson.jade.examples.bookTrading`, which is excluded.

<a name="6">\[6\]</a> If you directly downloaded t4j jar or if you built it from sources without using the provided ANT script, simply adjust the given command to suit your configuration.

<a name="7">\[7\]</a> If you do not want to use the command prompt to launch Java applications, adjust the given command to suit your configuration, e.g., if your are using [Eclipse IDE](http://www.eclipse.org/home/index.php): right-click on "jade.jar > Run As > Run Configurations..." then double-click on "Java Application", select "Boot - jade" as the main class, finally in the arguments tab put `-gui -services it.unibo.tucson.jade.service.TucsonService` as program arguments (`-cp t4j.jar:../libs/tucson.jar:../libs/2p.jar:../libs/jade.jar` is automatically added by Eclipse according to project's build path settings).

<a name="8">\[8\]</a> Separator `:` works on Mac & Linux only, use `;` on Windows.

<a name="9">\[9\]</a> Actually, a TuCSoN agent identifier can be any valid tuProlog *ground term*. See tuProlog documentation, [here](http://apice.unibo.it/xwiki/bin/download/Tuprolog/Download/tuprolog-guide-2.9.0.pdf).

<a name="10">\[10\]</a> Because the seller agent starts the TuCSoN node, the whole MAS runs in the same JVM process, thus sharing the same standard output. If you want to better distinguish what the agents do from what the TuCSoN node does, comment out node starting/stopping code in seller agent's source code and start the TuCSoN node separately <a href="#11">\[11\]</a>.

<a name="11">\[11\]</a> E.g., in a command prompt type `java -cp tucson.jar:2p.jar alice.tucson.service.TucsonNodeService`. More on TuCSoN in its documentation, [here](http://www.slideshare.net/andreaomicini/the-tucson-coordination-model-technology-a-guide).

---